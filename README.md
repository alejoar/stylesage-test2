# stylesage-test2
Job interview test for StyleSage

## Test requirements
1. Download this CSV: http://54.197.212.209:6801/static/products.zip
2. Write a small app that publish an API Rest with these endpoints:
    - Give me 20 products ordered by price.
    - Give me 20 products ordered by discount.
    - Give me the most discounted 20 products.
    - Give me all products with colour red or any other color.

## Docker
To launch the app with docker:

```
$ docker-compose up
```

Run the ingest command to have data available in the API:

```
$ docker exec -it stylesage-test2_api_1 flask ingest data/products.csv
```

Test (examples using [httpie](hhttps://httpie.org)):

```
http http://localhost/products/
```

## Endpoints

### /products/

A paginated list of all ingested data. Possible query params include:

#### sort

Ascending sort by any field. Use '-' prefix for descending.

```
http http://localhost/products/?sort=-brand
```

#### field filters
Filter almost any field.

```
http http://localhost/products/?brand=vans
```

#### color/icolor
Filter color by exact (color) or fuzzy (icolor)

```
http http://localhost/products/?color=blue
```


#### /products/<int:id> & /product-sku/<int:base_sku>

Get product by ID or list of products by SKU

```
http http://localhost/products/165320
http http://localhost/product-sku/40697407
```

#### /most-discounted/
Most discounted products (in amount of times discounted)


#### /more-discounted/
More discounted products (in value)

## Test solutions:
- Give me 20 products ordered by price

Ascending:
```
http://localhost/products/?sort=-_current_price_value
```
Descending:
```
http://localhost/products/?sort=-_current_price_value
```

- Give me 20 products ordered by discount.

Ascending:
```
http://localhost/products/?sort=discount
```
Descending:
```
http://localhost/products/?sort=-discount
```

- Give me the most discounted 20 products.

It is not clear if the premise refers to products discounted most times (1), or the products where the total sum of all discounts is greatest (2).

Solution for both cases:
##### (1)
```
http http://localhost/most-discounted/
```

##### (2)
```
http http://localhost/more-discounted/
```

- Give me all products with colour red or any other color.
```
http http://localhost/products/?color=red
```
