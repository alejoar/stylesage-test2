import unittest
import json

from flask import Flask
from flask_testing import TestCase
from flask_sqlalchemy import SQLAlchemy

from stylesage import create_app
from stylesage.models import db


class StyleSageTest(TestCase):

    def create_app(self):
        test_config = {
            'TESTING': True,
            'SQLALCHEMY_DATABASE_URI': "sqlite://",
            'SQLALCHEMY_TRACK_MODIFICATIONS': False
        }
        app = create_app(test_config)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()


class ViewsTest(StyleSageTest):

    def test_views(self):
        response = self.client.get("/")
        self.assert404(response)
        response = self.client.get("/products/")
        self.assert200(response)
        response = self.client.get("/most-discounted/")
        self.assert200(response)
        response = self.client.get("/more-discounted/")
        self.assert200(response)
        expected_reponse = {
            "count": 0,
            "data": [],
            "next": None,
            "prev": None
        }
        self.assertEqual(json.loads(response.data.decode()), expected_reponse)


if __name__ == '__main__':
    unittest.main()
