from flask import jsonify, request
from sqlalchemy import func

from stylesage.models import Product, product_schema, products_schema, db
from stylesage.api import api
from stylesage.api.utils import (paginate_data, sort_query, build_paginated_response,
                           filter_query, color_filter_query)


@api.route('/product-sku/<int:base_sku>')
def product_sku(base_sku):
    """List all data for a given SKU"""
    query = Product.query.filter_by(base_sku=str(base_sku))
    page, per_page = paginate_data(request)
    query = sort_query(request, Product, query)
    pagination = query.paginate(page, per_page=per_page)

    return jsonify(build_paginated_response(request,
                                            pagination,
                                            products_schema))


@api.route('/products/<int:id>')
def product_self(id):
    """Single Product element"""
    product = Product.query.get(id)
    data, errors = product_schema.dump(product)
    return jsonify(data)


@api.route('/products/')
def products():
    """List all products"""
    query = Product.query
    page, per_page = paginate_data(request)
    query = color_filter_query(request, Product, query)
    query = filter_query(request, Product, query)
    query = sort_query(request, Product, query)
    pagination = query.paginate(page, per_page=per_page)

    return jsonify(build_paginated_response(request,
                                            pagination,
                                            products_schema))


@api.route('/more-discounted/')
def more_discounted():
    """More discounted products (in value)"""
    query = db.session.query(
        Product.base_sku.label('base_sku'),
        func.sum(Product.discount).label('total_discount')
        ).group_by(Product.base_sku).order_by('total_discount DESC')

    page, per_page = paginate_data(request)

    query = color_filter_query(request, Product, query)
    query = filter_query(request, Product, query)
    query = sort_query(request, Product, query)

    pagination = query.paginate(page, per_page=per_page)

    result = build_paginated_response(request, pagination)

    data, errors = products_schema.dump([Product.query.filter_by(
            base_sku=bs).first() for bs, sum in pagination.items])

    for i, (key, value) in enumerate(pagination.items):
        data[i].update({'total_sku_discount': value})

    result.update({'data': data})
    return jsonify(result)


@api.route('/most-discounted/')
def most_discounted():
    """Most discounted products (in amount of times discounted)"""
    query = db.session.query(
        Product.base_sku.label('base_sku'),
        func.count(Product.base_sku).label('total')
        ).group_by(Product.base_sku).filter(
            Product.discount > 0).order_by('total DESC')

    page, per_page = paginate_data(request)

    query = color_filter_query(request, Product, query)
    query = filter_query(request, Product, query)
    query = sort_query(request, Product, query)

    pagination = query.paginate(page, per_page=per_page)

    result = build_paginated_response(request, pagination)

    data, errors = products_schema.dump([Product.query.filter_by(
            base_sku=bs).first() for bs, sum in pagination.items])

    for i, (key, value) in enumerate(pagination.items):
        data[i].update({'total_times_discounted': value})

    result.update({'data': data})
    return jsonify(result)
