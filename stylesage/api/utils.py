import inspect

from flask import url_for, current_app

from stylesage.models import ProductColor, Color


DONT_FILTER = ['color', 'icolor']


def paginate_data(request):
    page = request.args.get('page', 1, type=int)
    per_page = current_app.config.get('ITEMS_PER_PAGE', 20)

    if page < 1:
        page = 1

    start = per_page*(page-1)
    end = start+per_page

    # return page, per_page, start, end
    return page, per_page


def sort_query(request, model, query):
    attr = request.args.get('sort', '*')

    if not attr.startswith('_'):
        if attr.startswith('-'):
            desc = True
            attr = attr[1:]
        else:
            desc = False

        if hasattr(model, attr):
            attr = getattr(model, attr)

            if inspect.isfunction(attr):
                return query

            if desc:
                query = query.order_by(attr.desc())
            else:
                query = query.order_by(attr)

    return query


def filter_query(request, model, query):
    params = dict(request.args)
    params.pop('page', None)
    params.pop('sort', None)
    for key, value in params.items():
        if key in DONT_FILTER:
            continue
        elif hasattr(model, key) and not key.startswith('_'):
            query = query.filter(getattr(model, key) == value[0])
    return query


def color_filter_query(request, model, query):
    params = dict(request.args)
    color = params.pop('color', None)
    icolor = params.pop('icolor', None)
    if color:
        return query.join(ProductColor, Color).filter(Color.name == color[0])
    if icolor:
        return query.join(ProductColor, Color).filter(Color.name.like(icolor[0]))
    return query


def build_paginated_response(request, pagination, schema=None):
    args = {k: v for (k, v) in request.args.items() if k != 'page'}

    prev_url = None
    if pagination.has_prev:
        prev_url = url_for(request.endpoint, page=pagination.page - 1,
                           _external=True, **{**request.view_args, **args})
    next_url = None
    if pagination.has_next:
        next_url = url_for(request.endpoint, page=pagination.page + 1,
                           _external=True, **{**request.view_args, **args})

    if schema:
        data, errors = schema.dump(pagination.items)
    else:
        data = None

    return {
        'count': pagination.total,
        'data': data,
        'next': next_url,
        'prev': prev_url
    }
