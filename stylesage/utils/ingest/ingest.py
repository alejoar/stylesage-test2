import csv

import click
from flask import cli
from sqlalchemy.exc import IntegrityError

from stylesage.utils import get_or_create
from stylesage.models import db, Color, product_schema


class IngestException(Exception):
    pass


class Ingest(object):
    """Meant for loading all the CSV data into the DB"""

    def __init__(self, app=None):
        self.path = ''

    def init_app(self, app):
        try:
            self.path = app.config['INGEST_PATH']
        except KeyError:
            raise IngestException('INGEST_PATH not set.')

        app.cli.add_command(load_data)

    def load_data(self, path=None):
        self.path = path or self.path

        with open(self.path) as infile:
            reader = csv.DictReader(infile, delimiter=',', quotechar='"')
            i = 0
            objects = []

            print('Ingesting data..')
            for row in reader:
                i += 1
                product = self.ingest_product(row, commit=False)
                colors = []
                commit_colors = False
                for color_name in row['color_name'].split('/'):
                    if color_name:
                        color_name = color_name.lower().strip()
                        color, created = get_or_create(session=db.session,
                                                       model=Color,
                                                       commit=False,
                                                       name=color_name)
                        colors.append(color)
                        if created:
                            commit_colors = True

                if commit_colors:
                    db.session.bulk_save_objects(colors)
                    db.session.commit()

                product.colors = colors

                objects.append(product)
                if i % 1500 == 0:
                    print(i)
                    db.session.bulk_save_objects(objects)
                    try:
                        db.session.commit()
                    except IntegrityError:
                        db.session.rollback()
                        db.session.commit()
                    del objects
                    objects = []
            db.session.bulk_save_objects(objects)
            db.session.commit()
            print(i)

    @staticmethod
    def ingest_product(data, commit=True):
        product, e = product_schema.load(data)
        product.discount = (product._original_price_value -
                            product._current_price_value)

        if commit:
            db.session.add(product)
            db.session.commit()

        return product


@click.command('ingest')
@click.argument('path')
@cli.with_appcontext
def load_data(path=None):
    db.drop_all()
    db.create_all()
    ingest = Ingest()
    ingest.load_data(path)


ingest = Ingest()
