
from sqlalchemy.exc import IntegrityError

def get_or_create(session, model, commit=False, **kwargs):
    """Django style get_or_create method"""
    try:
        instance = model.query.filter_by(**kwargs).first()
    except IntegrityError:
        session.rollback()
        return get_or_create(session, model, commit=False, **kwargs)
    if instance:
        return instance, False
    else:
        instance = model(**kwargs)
        if commit:
            session.add(instance)
            session.commit()
        return instance, True
