import ast

from sqlalchemy import event

from stylesage.models import db
from stylesage.models.product_color import ProductColor


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    base_sku = db.Column(db.String(50))  # check...
    description_text = db.Column(db.Text)  # check...
    identifier = db.Column(db.Text)  # check...
    title = db.Column(db.Text)
    url = db.Column(db.String(200))
    _current_price_value = db.Column(db.Float)
    _original_price_value = db.Column(db.Float)
    timestamp = db.Column(db.DateTime)
    image_urls = db.Column(db.Text)
    category_names = db.Column(db.Text)
    size_infos = db.Column(db.Text)
    brand = db.Column(db.String(200))
    color_name = db.Column(db.String(200))
    country_code = db.Column(db.String(10))
    currency = db.Column(db.String(10))
    gender_names = db.Column(db.String(200))
    discount = db.Column(db.Float, index=True)

    colors = db.relationship('Color', secondary=ProductColor.__tablename__,
                             back_populates="products", lazy="dynamic")

    def __repr__(self):
        return '<Product {}:{}>'.format(self.title, self.id)

    @property
    def images(self):
        try:
            return self.image_urls.split(',')
        except (SyntaxError, ValueError):
            return None

    @property
    def size_info(self):
        try:
            return ast.literal_eval(self.size_infos)
        except (SyntaxError, ValueError):
            return None

    @property
    def categories(self):
        try:
            return self.category_names.split(',')
        except (SyntaxError, ValueError):
            return None


@event.listens_for(Product, 'before_insert')
@event.listens_for(Product, 'before_update')
def before_insert(mapper, connection, target):
    try:
        target.discount = (target._original_price_value -
                           target._current_price_value)
    except TypeError:
        # Either original or current price are missing from the object
        pass
