from stylesage.models import ma, Color, Product


class ColorSchema(ma.ModelSchema):

    class Meta:
        model = Color
        fields = ('name',)

color_schema = ColorSchema()
colors_schema = ColorSchema(many=True)


class ProductSchema(ma.ModelSchema):
    class Meta:
        model = Product
        exclude = ('colors',)
        additional = ('images', 'size_info', 'categories')
        load_only = ('image_urls', 'size_infos',
                     'category_names')

    _links = ma.Hyperlinks({
        'self': ma.URLFor('api.product_self', id='<id>', _external=True),
        'sku': ma.URLFor('api.product_sku', base_sku='<base_sku>', _external=True),
        'collection': ma.URLFor('api.products', _external=True)
    })

product_schema = ProductSchema()
products_schema = ProductSchema(many=True)
