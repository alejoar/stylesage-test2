import time

import click
from flask.cli import with_appcontext
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

db = SQLAlchemy()
ma = Marshmallow()

from .color import Color
from .product import Product
from .product_color import ProductColor
from .schemas import (color_schema, colors_schema, product_schema,
                      products_schema)


def init_db():
    done = False
    while not done:
        try:
            db.create_all()
            done = True
        except:
            # DB not yet up!
            time.sleep(2)
            init_db()


@click.command('create_tables')
@with_appcontext
def create_tables():
    init_db()
