from stylesage.models import db


class ProductColor(db.Model):
    # Association table / many to many
    __tablename__ = 'product_color_association'
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    color_id = db.Column(db.Integer, db.ForeignKey('color.id'))
