from stylesage.models import db
from stylesage.models.product_color import ProductColor
from stylesage.models.product import Product

class Color(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))

    products = db.relationship('Product',
                               secondary=ProductColor.__tablename__,
                               back_populates="colors", lazy="dynamic")

    def __repr__(self):
        return '<Color {}:{}>'.format(self.name, self.id)
