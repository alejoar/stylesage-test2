from flask import Flask


def create_app(config=None):
    app = Flask(__name__)
    if config:
        app.config.from_object(config)
    else:
        db_uri = 'postgresql://postgres:postgres@db:5432/postgres'
        app.config.from_mapping(
            SQLALCHEMY_DATABASE_URI=db_uri,
            SQLALCHEMY_TRACK_MODIFICATIONS=False
        )

    # Module initializations
    from stylesage.models import db, ma
    db = db.init_app(app)
    ma = ma.init_app(app)

    # Blueprints
    from stylesage.api import api as api_blueprint
    app.register_blueprint(api_blueprint)

    # Commands
    from stylesage.models import create_tables
    from stylesage.utils.ingest import load_data
    app.cli.add_command(load_data)
    app.cli.add_command(create_tables)

    # Shell context
    from stylesage.models import Product, Color, ProductColor, db

    @app.shell_context_processor
    def make_shell_context():
        return {
            'db': db,
            'Product': Product,
            'Color': Color,
            'ProductColor': ProductColor
        }

    return app
