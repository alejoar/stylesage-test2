FROM python:3
COPY requirements.txt /stylesage/requirements.txt
WORKDIR /stylesage
RUN pip install -r requirements.txt
# COPY . /stylesage
ADD . /stylesage
EXPOSE 5000
